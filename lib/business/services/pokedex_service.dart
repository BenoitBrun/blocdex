import 'dart:convert';

import 'package:blocdex/business/models/pokemon.dart';
import 'package:flutter/services.dart';

class PokedexService {
  Future<List<Pokemon>> getPokemons() async {
    List<Pokemon> results = [];
    for (Map e
        in jsonDecode(await rootBundle.loadString("assets/data/pokemons.json"))) {
      results.add(Pokemon(e['id'].toString(), e['name']['english'].toString(), TypeHelper.fromStringList(e['type'])));
    }
    results = results.getRange(0, 151).toList();
    return Future.delayed(Duration(seconds: 1)).then((value) => results);
  }
}
