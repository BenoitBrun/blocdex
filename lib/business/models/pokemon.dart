import 'package:flutter/cupertino.dart';

class Pokemon {
  final String id;
  final String name;
  final List<PokemonType> types;

  String get spritePath {
    return "assets/sprites/$id.png";
  }

  Pokemon(this.id, this.name, this.types);
}

enum PokemonType {
  Normal,
  Fire,
  Water,
  Grass,
  Electric,
  Ice,
  Fighting,
  Poison,
  Ground,
  Flying,
  Psychic,
  Bug,
  Rock,
  Ghost,
  Dark,
  Dragon,
  Steel,
  Fairy
}

extension TypeHelper on PokemonType {

  static PokemonType fromString(String s) {
    return PokemonType.values.firstWhere((element) => element.name == s);
  }

  static List<PokemonType> fromStringList(List<dynamic> typeList) {
    return typeList.map((e) => fromString(e)).toList();
  }

  String get name {
    return this.toString().split(".").last;
  }

  Color get color {
    switch (this) {
      case PokemonType.Fire:
        return Color(0xffffEE8130);
      case PokemonType.Water:
        return Color(0xff6390F0);
      case PokemonType.Grass:
        return Color(0xff7AC74C);
      case PokemonType.Electric:
        return Color(0xffF7D02C);
      case PokemonType.Ice:
        return Color(0xff96D9D6);
      case PokemonType.Fighting:
        return Color(0xffC22E28);
      case PokemonType.Poison:
        return Color(0xffA33EA1);
      case PokemonType.Ground:
        return Color(0xffE2BF65);
      case PokemonType.Flying:
        return Color(0xffA98FF3);
      case PokemonType.Psychic:
        return Color(0xffF95587);
      case PokemonType.Bug:
        return Color(0xffA6B91A);
      case PokemonType.Rock:
        return Color(0xffB6A136);
      case PokemonType.Ghost:
        return Color(0xff735797);
      case PokemonType.Dark:
        return Color(0xff705746);
      case PokemonType.Dragon:
        return Color(0xff6F35FC);
      case PokemonType.Steel:
        return Color(0xffB7B7CE);
      case PokemonType.Fairy:
        return Color(0xffD685AD);
      case PokemonType.Normal:
        return Color(0xffA8A77A);
    }
  }
}
