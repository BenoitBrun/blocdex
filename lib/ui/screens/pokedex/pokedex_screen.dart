import 'package:blocdex/business/models/pokemon.dart';
import 'package:blocdex/business/services/pokedex_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class PokedexScreen extends StatelessWidget {
  const PokedexScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pokedex'),
      ),
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: FutureBuilder<List<Pokemon>>(
                  future: PokedexService().getPokemons(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return _PokemonListWidget(snapshot.data);
                    } else {
                      return Center(child: SpinKitRotatingPlain(color: Colors.red,));
                    }
                  }),
            ),
          ),
        ],
      ),
    );
  }
}

class _PokemonListWidget extends StatelessWidget {
  final List<Pokemon>? _pokemonList;

  const _PokemonListWidget(this._pokemonList, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const BouncingScrollPhysics(),
      itemCount: _pokemonList?.length,
      itemBuilder: (context, index) => _PokemonItemWidget(_pokemonList![index]),
    );
  }
}

class _PokemonItemWidget extends StatelessWidget {
  final Pokemon _pokemon;

  const _PokemonItemWidget(this._pokemon, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: ListTile(
          leading: Image.asset(
            _pokemon.spritePath,
          ),
          title: Text(_pokemon.name, style: TextStyle(fontSize: 24, color: Colors.black38, fontWeight: FontWeight.bold),),
          subtitle: _PokemonTypeWidget(_pokemon.types),
          trailing: Text("# ${_pokemon.id}", style: TextStyle(fontSize: 32, color: Colors.black12),),
          isThreeLine: true,
        ),
      ),
    );
  }
}

class _PokemonTypeWidget extends StatelessWidget {
  final List<PokemonType> _types;

  const _PokemonTypeWidget(this._types, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemCount: _types.length,
          separatorBuilder: (context, index) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: const Icon(
                  Icons.circle,
                  size: 5,
                  color: Colors.black12,
                ),
              ),
          itemBuilder: (context, index) {
            return Chip(
              visualDensity: VisualDensity.compact,
              label: Text(
                _types[index].name,
                style: const TextStyle(color: Colors.white, fontSize: 12),
              ),
              backgroundColor: _types[index].color,
            );
          }),
    );
  }
}
